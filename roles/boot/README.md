boot role
=========

Boots machines in the 3-machine cluster.

Requirements
------------
No requirements besides Ansible.

Role Variables
--------------

No role variables.

Dependencies
------------

* `community.general` collection for Wake-On-Lan.

Example Playbook
----------------

    - hosts: servers
      roles:
         - boot

License
-------

MIT

Author Information
------------------

Ian S. McBride <ian.s.mcb@gmail>
