hostname role
=========

Set a machines hostname and /etc/hosts file.

Requirements
------------

No requirements besides Ansible.

Role Variables
--------------

No role variables.

Dependencies
------------

No dependencies.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - hostname

License
-------

MIT

Author Information
------------------

Ian S. McBride <ian.s.mcb@gmail>
