shutdown role
=========

Shuts down machines.

Requirements
------------
No requirements besides Ansible.

Role Variables
--------------

No role variables.

Dependencies
------------

No dependencies.

Example Playbook
----------------

    - hosts: servers
      roles:
         - shutdown

License
-------

MIT

Author Information
------------------

Ian S. McBride <ian.s.mcb@gmail>
