network role
=========

Configures network firewall and bridged traffic settings.

Requirements
------------
No requirements besides Ansible.

Role Variables
--------------

No role variables.

Dependencies
------------

No dependencies.

Example Playbook
----------------

    - hosts: servers
      roles:
         - network

License
-------

MIT

Author Information
------------------

Ian S. McBride <ian.s.mcb@gmail>
