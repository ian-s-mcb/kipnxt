filesystems role
=========

Sets filesystems to be mounted.

Requirements
------------
No requirements besides Ansible.

Role Variables
--------------

* sys_uuid - System partition
* esp_uuid - EFI System partition
* storage_uuid - Aux storage partition
* storage_mount_point - Aux storage partition mount point

Dependencies
------------

No dependencies.

Example Playbook
----------------

    - hosts: servers
      roles:
         - filesystems

License
-------

MIT

Author Information
------------------

Ian S. McBride <ian.s.mcb@gmail>
