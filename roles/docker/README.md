docker role
=========

Installs Docker.

Requirements
------------
No requirements besides Ansible.

Role Variables
--------------

No role variables.

Dependencies
------------

No depencies.

Example Playbook
----------------

    - hosts: servers
      roles:
         - docker

License
-------

MIT

Author Information
------------------

Ian S. McBride <ian.s.mcb@gmail>
