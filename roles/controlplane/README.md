Controlplane role
=========

Initializes cluster using kubeadm, sets config for cluster cli tools,
and installs the pod network add-on (calico).

Requirements
------------
No requirements besides Ansible.

Role Variables
--------------

* username - the user who will run cluster cli tools
* cidr - the range of IPs for the virtual cluster network

Dependencies
------------

No dependencies

Example Playbook
----------------

    - hosts: servers
      roles:
         - controlplane

License
-------

MIT

Author Information
------------------

Ian S. McBride <ian.s.mcb@gmail>
