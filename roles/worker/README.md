worker role
=========

Joins machine to cluster via a kubeadm command stored in
`files/join.sh`. This file is created by the `controlplane` role.

Requirements
------------
No requirements besides Ansible.

Role Variables
--------------

No role variables.

Dependencies
------------

No dependencies.

Example Playbook
----------------

    - hosts: servers
      roles:
         - worker

License
-------

MIT

Author Information
------------------

Ian S. McBride <ian.s.mcb@gmail>
