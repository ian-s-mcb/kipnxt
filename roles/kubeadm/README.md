kubeadm role
=========

Installs kubadm.

Requirements
------------

No requirements besides Ansible.

Role Variables
--------------

No role variables.

Dependencies
------------

No dependencies.

Example Playbook
----------------

    - hosts: servers
      roles:
         - kubeadm

License
-------

MIT

Author Information
------------------

Ian S. McBride <ian.s.mcb@gmail>
