misc role
=========

Sets user password and shortens grub menu timeout.

Requirements
------------
No requirements besides Ansible.

Role Variables
--------------

- username
- password
- vault_password

Dependencies
------------

No dependencies.

Example Playbook
----------------

    - hosts: servers
      roles:
         - misc

License
-------

MIT

Author Information
------------------

Ian S. McBride <ian.s.mcb@gmail>
