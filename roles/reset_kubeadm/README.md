reset_kubeadm role
=========

Resets the kubeadm init operation.

Requirements
------------

No requirements besides Ansible.

Role Variables
--------------

No role variables.

Dependencies
------------

No dependencies

Example Playbook
----------------

    - hosts: servers
      roles:
         - reset_kubeadm

License
-------

MIT

Author Information
------------------

Ian S. McBride <ian.s.mcb@gmail>
