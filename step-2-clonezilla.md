# step-2-clonezilla.sh

Remember to boot Clonezilla with the option for large font and RAM.

Clonezilla is used to perform two operations: 1) clone disk to image and
2) restore image to disk. Both can be performed with the "beginner
mode," which is series of interative prompts. The answers to those
prompts are given below. Alternatively, the operations can be performed
by running **long** commands. Those commands are listed here too.

## Step-1 - Backup `fiddlesticks` image
- Disk-to-image
- NFS server
- DHCP IP
- NFS v4
- 192.168.1.105
- /mnt/nas/videos/images
- Beginner mode
- Save local disk as an image
- Accept default image name
- Pick disk to clone
- Parellel gzip compression
- Skip checking/repairing
- Yes, check the saved image
- No, don't encrypt
- Poweroff when finished

### Alternative command
`/usr/sbin/ocs-sr -q2 -c -j2 -z1p -i 4096 -sfsck -senc -p poweroff savedisk 2021-11-04-fiddlesticks-img nvme0n1`
   
## Step-2 - Restore image on other cluster machines
- Disk-to-image
- NFS server
- DHCP IP
- NFS v4
- 192.168.1.105
- /mnt/nas/videos/images
- Beginner mode
- Restore an image to a local disk
- Pick image
- Pick target disk
- No, skip checking the image before restoring
- Poweroff when finished

### Alternative command
`/usr/sbin/ocs-sr -g auto -e1 auto -e2 -r -j2 -c -scr -p poweroff restoredisk 2021-11-04-fiddlesticks-img nvme0n1`

## Step-3 - Fix bootloader for Ubuntu
- Boot into Windows (will cause Ubuntu not to boot afterwards)
- Boot into a Live Ubuntu installer (switch to shell)
- Chroot into installed Ubuntu
- Reinstall Grub
```bash
mount <ROOT_DEVICE> /mnt
mount <ESP_DEVICE> /mnt/boot/efi
for i in /dev /dev/pts /proc /sys /run; do mount -B $i /mnt$i; done
grub-install /dev/nvme0n1
```
