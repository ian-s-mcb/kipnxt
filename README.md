# kipnxt

A couple [Ansible playbooks][ansible] that bootstrap a 3-machine,
on-premise, baremetal [Kubernetes][k8s] cluster.

I built `kipnxt` to learn about Ansible and K8s, and even though it does
work, I don't recommend using it to provision your own cluster.
Instead, you should look into [kubespray][kubespray], which is the
official, community-maintained Ansible installer for K8s. When I built
`kipnxt`, `kubespray` was behind on supporting the latest features in
Ansible and too complicated for my liking, so I decided to roll my own
installer.

### A few assumptions

* K8s ver.1.22.x.
* [Ubuntu 20.04 (focal)][ubuntu] as host.
* Host machines (and their MAC addresses) are listed in `inventory.yml`.
* Disk contains three partitions ([ESP][esp], root, storage).
* Primary user's password hash is stored in `roles/misc/vars/vault.yml`
  using [ansible-vault][ansible-vault].

### Prerequisites 

1. Update inventory file.
1. Place username in `custom_vars.yml`.
1. Update vault file `roles/misc/vars/vault.yml`:
    * Replace file contents with one variable named `vault_password`
      with the value of your password hash.
    * Don't have a password hash? Run this
      [Python one-liner described here][ansible-gen-hash].
    * Create and apply a password for vaulting the var file:
      `ansible-vault encrypt --vault-id cluster@prompt roles/misc/vars/vault.yml`
    * Use your vaulting password when running playbooks
    * Full explanations of these steps can be found
      [here][ansible-vault-encrypt].

## Run kipnxt

* Read and follow steps 1, 2. The result will be three Ubuntu machines
  ready for control with Ansible
* Run step-3
  `ansible-playbook -i inventory.yml --vault-id cluster@prompt step-3-cluster.yml`

### Extra playbooks

The two "extra" playbooks can be used to either boot or shutdown the
cluster. The Ansible argument for vaulting isn't required for these
playbooks.

[k8s]: https://github.com/kubernetes/kubernetes
[ansible]: https://github.com/ansible/ansible
[kubespray]: https://github.com/kubernetes-sigs/kubespray
[ubuntu]: http://releases.ubuntu.com/
[esp]: https://wiki.archlinux.org/index.php/EFI_system_partition
[ansible-gen-hash]: https://docs.ansible.com/ansible/latest/reference_appendices/faq.html#how-do-i-generate-encrypted-passwords-for-the-user-module
[ansible-vault]: https://docs.ansible.com/ansible/latest/user_guide/vault.html
[ansible-vault-encrypt]: https://docs.ansible.com/ansible/latest/user_guide/vault.html#editing-encrypted-files
