# step-1-manual.sh

Set up the machine (named `fiddlesticks`) whose disk image will be
cloned onto the remaining cluster machines. Installed on this machine
will be Ubuntu server and (optionally) Windows 10. Having Windows
installed, along side Linux, makes it easier troubleshoot
hardware/firmware issues, but it's not strictly needed for kipnxt.

## Step-1 - (Optional) Install Windows
- Boot into Win10 installer
- Delete all partitions, create a new 64 GiB partition (will trigger the
  creation of all partitions needed for Windows)
- Boot into Windows
    * First user: admin, offline
    * Second user: ian.s.mcb@gmail.com
    * Install, configure Firefox
    * Install updates

## Step-2 - Install Ubuntu server
- Boot into Ubuntu installer
- Partitions
    * Switch into shell (via help on the top right menu bar)
    * Create a 64 GiB partition (#5) via fdisk
    * Create a partition (#6) with remaining space via fdisk
- Return to installer menu, install OS
    * Username: ian
    * Hostname: fiddlesticks
    * Enable openssh-server

## Step-3 - Configure SSH access
- Boot Ubuntu server
- Run this from a second machine on the same LAN
- Copy ssh key, ssh into Ubuntu server
```bash
ssh-copy-id -i ~/path/to/private/key fiddlesticks
ssh fiddlesticks
```

## Step-4 - (Inside Ubuntu server) Label partitions, fix sudo
- Enable passwordless sudo for members of the sudo group
- Set partition labels (to be used in /etc/fstab)
```bash
echo '%sudo ALL=(ALL:ALL) NOPASSWD:ALL' | sudo EDITOR='tee -a' visudo 
sudo umount /dev/nvme0n1p1
sudo fatlabel /dev/nvme0n1p1 ESP
sudo mount -a
sudo e2label /dev/nvme0n1p5 ROOT
sudo e2label /dev/nvme0n1p6 STORAGE
sudo blkid # Inspect partition labels
```
